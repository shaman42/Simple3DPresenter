#include <DisplayWindow.h>

#include <qDebug>

DisplayWindow::DisplayWindow(const QString& configFile, 
    Structure::Eye eye, quint16 port, 
    QWidget* parent)
: QMainWindow(parent)
, tcpServer(nullptr)
, tcpSocket(nullptr)
, eye(eye)
, structure(configFile)
{
    qDebug() << "Adresses:";
    for(const QHostAddress &address : QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && !address.isLoopback())
            qDebug() << address.toString();
    }

    setWindowTitle(eye == Structure::Eye::Left ? "Display Left" : "Display Right");
    setMinimumSize(400, 300);

    label = new QLabel(this);
    label->setScaledContents(true);
    label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    setCentralWidget(label);

    qDebug() << "Start thread";
    loadingThread = new LoadingThread(this, this);
    loadingThread->loadSequence(0);
    loadingThread->loadSequence(1);
    loadingThread->start();

    qDebug() << "Open Server for eye " << (eye == Structure::Eye::Left ? "'left'" : "'right'");
    tcpServer = new QTcpServer(this);
    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newConnection()));
    if (!tcpServer->listen(QHostAddress::Any, port))
    {
        qDebug() << "Unable to start server";
        exit(-1);
    }
    qDebug() << "Server opened";
}

void DisplayWindow::newConnection()
{
    tcpSocket = tcpServer->nextPendingConnection();
    tcpServer->pauseAccepting();
    qDebug() << "Connection for eye " << (eye == Structure::Eye::Left ? "'left'" : "'right'") << "established";
    connect(tcpSocket, &QIODevice::readyRead, this, &DisplayWindow::messageReceived);
    connect(tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
        this, &DisplayWindow::displayError);
    in.setDevice(tcpSocket);
    in.setVersion(QDataStream::Qt_5_0);
}

void DisplayWindow::messageReceived()
{
    while (true) {
        Message m;
        if (Message::readMessage(in, m))
        {
            //qDebug() << "Message " << static_cast<int>(m.type) << ":" << m.arg1 << "," << m.arg2 << " received";
            switch (m.type)
            {
            case Message::Type::Close:
            {
                tcpSocket->close();
                tcpServer->close();
                loadingThread->loadSequence(-2);
                loadingThread->wait();
                qDebug() << "closed";
                close();
            }break;
            case Message::Type::LoadSequence:
            {
                loadingThread->loadSequence(m.arg1);
            }break;
            case Message::Type::UnloadSequence:
            {
                qDebug() << "Unload sequence " << m.arg1;
                structure.unloadSequence(&structure.sequences[m.arg1]);
            }break;
            case Message::Type::ShowImage:
            {
                if (m.arg1<0 || m.arg1>=structure.sequences.size()) break;
                if (!structure.sequences[m.arg1].loaded) { qDebug() << "not loaded!"; break; }
                if (m.arg2 < 0 || m.arg2 >= structure.sequences[m.arg1].images.size()) break;
                currentImg = structure.sequences[m.arg1].images[m.arg2];
                label->setPixmap(currentImg);

                Message ret = { Message::Type::ImageShown, m.arg1, m.arg2 };
                Message::writeMessage(tcpSocket, ret);
            }break;
            case Message::Type::IsLoadedRequest:
            {
                bool loaded = structure.sequences[m.arg1].loaded;
                if (m.arg1 + 1 < structure.sequences.size()) loaded = loaded && structure.sequences[m.arg1+1].loaded;
                Message ret = { Message::Type::IsLoadedAnswer, loaded ? 1 : 0, 0 };
                Message::writeMessage(tcpSocket, ret);
            }break;
            }
        }
        else
        {
            return;
        }
    }
}

void DisplayWindow::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The host was not found. Please check the "
                "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The connection was refused by the peer. "
                "Make sure the fortune server is running, "
                "and check that the host name and port "
                "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The following error occurred: %1.")
            .arg(tcpSocket->errorString()));
    }
}

void DisplayWindow::closeEvent(QCloseEvent* bar)
{
    if (tcpSocket) tcpSocket->close();
    if (tcpServer) tcpServer->close();
    if (loadingThread) loadingThread->loadSequence(-2);
    if (loadingThread && loadingThread->isRunning()) loadingThread->wait();
    qDebug() << "closed";
    QMainWindow::closeEvent(bar);
}

DisplayWindow::LoadingThread::LoadingThread(DisplayWindow* manager, QObject* parent)
    : QThread(parent), manager(manager)
{
}

void DisplayWindow::LoadingThread::run()
{
    qDebug() << "Loading Thread started";
    while (true)
    {
        int toLoad;
        mutex.lock();
        if (nextSequence.empty())
            condition.wait(&mutex);
        toLoad = nextSequence.front(); nextSequence.pop();
        mutex.unlock();
        if (toLoad < 0)
        {
            qDebug() << "Shut down loading thread";
            return;
        }

        if (manager->structure.sequences.at(toLoad).loaded) continue;

        //load given sequence
        qDebug() << "Load sequence " << toLoad;
        manager->structure.loadSequence(&manager->structure.sequences[toLoad], manager->eye);
    }
}

void DisplayWindow::LoadingThread::loadSequence(int sequence)
{
    qDebug() << "Trigger loading of " << sequence;
    mutex.lock();
    if (sequence < 0)
        nextSequence.push(-2); //exit
    else
        nextSequence.push(sequence);
    condition.wakeAll();
    mutex.unlock();
}
