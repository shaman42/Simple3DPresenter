#include <QApplication>
#include <QCommandLineParser>
#include <QThread>
#include <qDebug>

#include <MainWindow.h>
#include <ControlWindow.h>
#include <DisplayWindow.h>
#include <omp.h>



int main(int argc, char* argv[]) {
	QApplication a(argc, argv);
	QCoreApplication::setOrganizationName("Shaman");
	QCoreApplication::setApplicationName("Simple 3D Presenter");
	QCoreApplication::setApplicationVersion(QT_VERSION_STR);

    omp_set_num_threads(std::max(1, QThread::idealThreadCount() - 2));

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addPositionalArgument("file", "Configuration file");
    parser.addPositionalArgument("mode", "Mode", "[left|right|control]");
    parser.addPositionalArgument("lefthost", "Left Host Name");
    parser.addPositionalArgument("righthost", "Right Host Name");
    QCommandLineOption fullscreenOption("f", "Fullscreen");
    parser.addOption(fullscreenOption);
    parser.process(a);
    const QStringList args = parser.positionalArguments();
    QString configFile = args.at(0);

    quint16 portLeft = 4454;
    quint16 portRight = 4455;

    QMainWindow* win = nullptr;

    if (args.size() == 1)
    {
        //test

        //launch left and right
        QString leftCommand = QString("start ") + argv[0] + " " + configFile + " " + "left";
        QString rightCommand = QString("start ") + argv[0] + " " + configFile + " " + "right";
        qDebug() << "launch left display: " << leftCommand;
        qDebug() << "launch right display" << rightCommand;
        QThread::sleep(1);
        system(leftCommand.toStdString().c_str());
        QThread::sleep(1);
        system(rightCommand.toStdString().c_str());
        QThread::sleep(1);

        //start main window
        ControlWindow* control = new ControlWindow(configFile, "localhost", portLeft, "localhost", portRight);
        control->show();
        win = control;
    } else
    {
        QString mode = args.at(1);
        if (mode == "left")
        {
            DisplayWindow* w = new DisplayWindow(configFile, Structure::Eye::Left, portLeft);
            if (parser.isSet(fullscreenOption))
                w->showFullScreen();
            else
                w->show();
            win = w;
        } else if (mode == "right")
        {
            DisplayWindow* w = new DisplayWindow(configFile, Structure::Eye::Right, portRight);
            if (parser.isSet(fullscreenOption))
                w->showFullScreen();
            else
                w->show();
            win = w;
        } else if (mode == "control")
        {
            ControlWindow* control = new ControlWindow(configFile, args.at(2), portLeft, args.at(3), portRight);
            if (parser.isSet(fullscreenOption))
                control->showFullScreen();
            else
                control->show();
            win = control;
        }
    }


    //Test

    /*

    QCommandLineParser parser;
    parser.addPositionalArgument("file", "Configuration file");
    parser.addPositionalArgument("eye", "Eye", "[left|right]");
    parser.process(a);
    const QStringList args = parser.positionalArguments();
    QString configFile = args.at(0);
    QString eye = (args.size() >= 2) ? args.at(1) : "";

	MainWindow window(configFile, eye);
	//window.showMaximized();
    window.setWindowModality(Qt::ApplicationModal);
    window.showFullScreen();

    */

    qDebug() << "Enter main loop";
	auto r = a.exec();
    delete win;
    return r;
}