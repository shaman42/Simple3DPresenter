#include <StructureManager.h>

#include <QDebug>
#include <QtXml>
#include <QElapsedTimer>
#include <omp.h>
#include <algorithm>

StructureManager::StructureManager(const QString& configFile)
    : timer(nullptr)
    , loadingThread(nullptr)
    , structure(configFile)
{
    qDebug() << "Load " << configFile;
    QDomDocument document;
    QFile file(configFile);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Failed to open the file for reading.";
        exit(-1);
    }
    else
    {
        // loading
        if (!document.setContent(&file))
        {
            qDebug() << "Failed to load the file for reading.";
            exit(-1);
        }
        file.close();
    }

    // Getting root element
    QDomElement root = document.firstChildElement();

    // Read include statements
    QDomNodeList nodes = root.elementsByTagName("include");
    for (int i = 0; i < nodes.count(); i++)
    {
        QDomNode elm = nodes.at(i);
        if (elm.isElement())
        {
            QDomElement e = elm.toElement();
            QString path = e.text();
            path = path.trimmed();
            if (!path.endsWith('/') || !path.endsWith('\\'))
                path = path + "\\";
            includePaths.push_back(path);
            qDebug() << "Include path: " << path;
        }
    }

    // Read sequences
    nodes = root.elementsByTagName("sequence");
    for (int i = 0; i < nodes.count(); i++)
    {
        QDomNode elm = nodes.at(i);
        if (elm.isElement())
        {
            QDomElement e = elm.toElement();
            Structure::Sequence s;
            s.filePattern = e.attribute("filePattern");
            s.start = e.attribute("start", "0").toInt();
            s.stop = e.attribute("stop", "0").toInt();
            s.fps = e.attribute("fps", "0").toInt();
            QString anim = e.attribute("anim", "");
            if (anim == "Repeat")
                s.animation = Structure::Animation::Repeat;
            else if (anim == "Mirror")
                s.animation = Structure::Animation::Mirror;
            else if (anim == "Once")
                s.animation = Structure::Animation::Once;
            else
                s.animation = Structure::Animation::Off;
            structure.sequences.push_back(s);
        }
    }
    loadedSequences = std::make_unique<std::vector<LoadedSequence>>(structure.sequences.size());
    currentSequence = 0;
    currentStep = 0;

    omp_set_num_threads(std::max(1, QThread::idealThreadCount() - 2));

    loadingThread = new LoadingThread(this, this);
    loadingThread->loadSequence(0);
    loadingThread->loadSequence(1);
}

void StructureManager::start()
{
    loadingThread->start();
    sendCurrentImage();
}

void StructureManager::setEye(Eye eye)
{
    this->eye = eye;
}

void StructureManager::clickLeft()
{
    qDebug() << "previous";

    if (currentSequence==0 && currentStep==structure.sequences[currentSequence].start)
    {
        return; //already at the beginning
    }

    //stop timer
    if (timer)
    {
        timer->stop();
        delete timer;
        timer = nullptr;
    }

    bool sequenceChanged = false;
    if (structure.sequences[currentSequence].animation != Structure::Animation::Off)
    {
        //stop animation
        currentSequence--;
        sequenceChanged = true;
    }
    else
    {
        currentStep--;
        if (structure.sequences[currentSequence].start > currentStep)
        {
            //done with that sequence
            currentSequence--;
            sequenceChanged = true;
        }
    }

    if (sequenceChanged)
    {
        //switch to previous sequence
        if (structure.sequences[currentSequence].animation != Structure::Animation::Off)
        {
            //jump to the start of that animation
            currentStep = structure.sequences[currentSequence].start;
            //start animation again?
            if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror
                || structure.sequences[currentSequence].animation == Structure::Animation::Repeat)
            {
                //start timer
                timer = new QTimer(this);
                timer->setTimerType(Qt::PreciseTimer);
                timer->setInterval(1000 / structure.sequences[currentSequence].fps);
                timer->setSingleShot(false);
                timerState = 0;
                connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
                timer->start();
            }
        } else
        {
            //jump to the end of that animation
            currentStep = structure.sequences[currentSequence].stop;
        }
    }

    sendCurrentImage();
}

void StructureManager::setOptions(int options)
{
    this->options = options;
}

void StructureManager::clickRight()
{
    qDebug() << "next";
    bool sequenceChanged = false;
    if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror
        || structure.sequences[currentSequence].animation == Structure::Animation::Repeat
        || (structure.sequences[currentSequence].animation == Structure::Animation::Once 
            && currentStep > structure.sequences[currentSequence].start))
    {
        //stop animation
        currentSequence++;
        sequenceChanged = true;
    } else
    {
        currentStep++;
        if (structure.sequences[currentSequence].stop < currentStep)
        {
            //done with that sequence
            currentSequence++;
            sequenceChanged = true;
        }
    }

    if (currentSequence >= structure.sequences.size())
    {
        currentSequence = structure.sequences.size() - 1;
        return;
    }

    if (sequenceChanged)
    {
        //stop timer
        if (timer)
        {
            timer->stop();
            delete timer;
            timer = nullptr;
        }

        //enter next sequence
        currentStep = structure.sequences[currentSequence].start;
        if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror
            || structure.sequences[currentSequence].animation == Structure::Animation::Repeat)
        {
            //start timer
            timer = new QTimer(this);
            timer->setTimerType(Qt::PreciseTimer);
            timer->setInterval(1000 / structure.sequences[currentSequence].fps);
            timer->setSingleShot(false);
            timerState = 0;
            connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
            timer->start();
        }
        //load next sequence
        if (structure.sequences.size() > currentSequence+1)
            loadingThread->loadSequence(currentSequence + 1);
    }
    if (currentStep == structure.sequences[currentSequence].start+1
        && structure.sequences[currentSequence].animation == Structure::Animation::Once)
    {
        //special case: start 'Once'-Animation from the second frame
        //start timer
        timer = new QTimer(this);
        timer->setTimerType(Qt::PreciseTimer);
        timer->setInterval(1000 / structure.sequences[currentSequence].fps);
        timer->setSingleShot(false);
        timerState = 0;
        connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
        timer->start();
    }

    sendCurrentImage();
}

void StructureManager::clickPauseResume()
{
    qDebug() << "pause/resume";
}

void StructureManager::close()
{
    loadingThread->loadSequence(-2);
    loadingThread->wait();
}

void StructureManager::timerUpdate()
{
    if (timerState == 0)
    {
        //forward
        if (structure.sequences[currentSequence].stop <= currentStep)
        {
            if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror)
            {
                timerState = 1;
                currentStep--;
            } 
            else if (structure.sequences[currentSequence].animation == Structure::Animation::Repeat)
            {
                currentStep = structure.sequences[currentSequence].start;
            } else
            {
                if (timer) {
                    timer->stop();
                    delete timer;
                    timer = nullptr;
                }
            }
        } else
        {
            currentStep++;
        }
    } else
    {
        //backward
        if (currentStep > structure.sequences[currentSequence].start)
        {
            currentStep--;
        } else
        {
            currentStep = structure.sequences[currentSequence].start+1;
            timerState = 0;
        }
    }
    sendCurrentImage();
}

void StructureManager::sendCurrentImage()
{
    LoadedSequence* ls = &loadedSequences->at(currentSequence);
    QPixmap img;
    ls->mutex.lock();
    if (!ls->loaded)
    {
        loadingThread->loadSequence(currentSequence);
        ls->condition.wait(&ls->mutex);
    }
    img = ls->images[static_cast<int>(eye)].at(currentStep - structure.sequences[currentSequence].start);
    ls->mutex.unlock();
    qDebug() << "Current image: " << currentSequence << " - " << currentStep;
    nextImage(img);
}

StructureManager::LoadingThread::LoadingThread(StructureManager* manager, QObject* parent)
: QThread(parent)
, manager(manager)
{
}

void StructureManager::LoadingThread::run()
{
    qDebug() << "Loading Thread started";
    int time = 1;
    int numLoaded = 0;
    int maxLoaded = 3;
    while(true)
    {
        int toLoad;
        mutex.lock();
        if (nextSequence.empty())
            condition.wait(&mutex);
        toLoad = nextSequence.front(); nextSequence.pop();
        mutex.unlock();
        if (toLoad < 0)
        {
            qDebug() << "Shut down loading thread";
            return;
        }

        if (manager->loadedSequences->at(toLoad).loaded) continue;

        if (numLoaded >= maxLoaded)
        {
            //unload oldest sequence
            int oldestAge = INT_MAX;
            int seq = -1;
            for (int i=0; i<manager->loadedSequences->size(); ++i)
            {
                if (manager->loadedSequences->at(i).loaded && manager->loadedSequences->at(i).time < oldestAge)
                {
                    seq = i;
                    oldestAge = manager->loadedSequences->at(i).time;
                }
            }
            if (seq >= 0)
            {
                qDebug() << "Unload sequence " << seq;
                manager->loadedSequences->at(seq).mutex.lock();
                manager->loadedSequences->at(seq).loaded = false;
                manager->loadedSequences->at(seq).images[0].clear();
                manager->loadedSequences->at(seq).images[1].clear();
                manager->loadedSequences->at(seq).mutex.unlock();
                numLoaded--;
            }
        }

        //load given sequence
        qDebug() << "Load sequence " << toLoad;
        LoadedSequence* ls = &manager->loadedSequences->at(toLoad);
        const Structure::Sequence& s = manager->structure.sequences[toLoad];
        ls->images[0].resize(s.stop - s.start + 1);
        ls->images[1].resize(s.stop - s.start + 1);
#pragma omp parallel for
        for (int i=s.start; i<=s.stop; ++i)
        {
            if (manager->options & Options::LoadLeft) ls->images[0][i-s.start] = load(s.filePattern, i, "left");
            if (manager->options & Options::LoadRight) ls->images[1][i - s.start] = load(s.filePattern, i, "right");
        }
        qDebug() << "Sequence " << toLoad << " loaded, " << (s.stop-s.start+1) << " images processed";
        numLoaded++;
        ls->time = time; time++;
        ls->mutex.lock();
        ls->loaded = true;
        ls->condition.wakeAll();
        ls->mutex.unlock();
    }
}

void StructureManager::LoadingThread::loadSequence(int sequence)
{
    qDebug() << "Trigger loading of " << sequence;
    mutex.lock();
    if (sequence < 0)
        nextSequence.push(-2); //exit
    else
        nextSequence.push(sequence);
    condition.wakeAll();
    mutex.unlock();
}

QPixmap StructureManager::LoadingThread::load(const QString & p, int index, const QString& eye)
{
    QElapsedTimer timer;
    timer.start();
    QString pattern = p;
    pattern = pattern.replace(QString("{eye}"), eye);
    QString path;
    path.sprintf(pattern.toStdString().c_str(), index);
    QString finalPath;
    for (QString incl : manager->includePaths)
    {
        QFile file(incl + path);
        if (file.exists())
        {
            finalPath = incl + path;
            break;
        }
    }
    QFile file(finalPath);
    if (file.exists())
    {
        //load that file
        qDebug() << "Load \"" << file.fileName() << "\"";
        QPixmap img(file.fileName());
        qDebug() << "Loaded in " << timer.elapsed() << "ms: " << img;
        return img;
    }
    else
    {
        qDebug() << "No file \"" << path << "\" found in the include paths";
        return QPixmap();
    }
}
