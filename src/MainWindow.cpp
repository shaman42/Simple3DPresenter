#include <MainWindow.h>

#include <QtDebug>

MainWindow::MainWindow(const QString& configFile, const QString& eye, QWidget* parent)
: QMainWindow(parent)
, m(configFile)
, label(new QLabel())
{
    label->setScaledContents(true);
    label->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    setCentralWidget(label);

    connect(&m, SIGNAL(nextImage(QPixmap)), this, SLOT(nextImage(QPixmap)));
    if (eye == "right")
    {
        m.setEye(StructureManager::Eye::Right);
        m.setOptions(StructureManager::LoadRight);
        qDebug() << "right eye";
    } else
    {
        m.setEye(StructureManager::Eye::Left);
        m.setOptions(StructureManager::LoadLeft);
        qDebug() << "left eye";
    }
    m.start();
    
    quitShortcut = new QShortcut(QKeySequence("Esc"), this);
    connect(quitShortcut, SIGNAL(activated()), &m, SLOT(close()));
    connect(quitShortcut, SIGNAL(activated()), this, SLOT(close()));
    previousShortcut = new QShortcut(QKeySequence("Left"), this);
    connect(previousShortcut, SIGNAL(activated()), &m, SLOT(clickLeft()));
    nextShortcut = new QShortcut(QKeySequence("Right"), this);
    connect(nextShortcut, SIGNAL(activated()), &m, SLOT(clickRight()));
    spaceShortcut = new QShortcut(QKeySequence("Space"), this);
    connect(spaceShortcut, SIGNAL(activated()), &m, SLOT(clickPauseResume()));
}

void MainWindow::nextImage(QPixmap img)
{
    currentImg = img;
    label->setPixmap(img);
    qDebug() << "label updated: " << img;
}
