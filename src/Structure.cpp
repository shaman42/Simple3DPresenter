#include "Structure.h"

#include <QtXml>
#include <QDebug>

Structure::Structure(const QString& configFile)
{
    qDebug() << "Load " << configFile;
    QDomDocument document;
    QFile file(configFile);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Failed to open the file for reading.";
        exit(-1);
    }
    else
    {
        // loading
        if (!document.setContent(&file))
        {
            qDebug() << "Failed to load the file for reading.";
            exit(-1);
        }
        file.close();
    }

    // Getting root element
    QDomElement root = document.firstChildElement();

    // Read include statements
    QDomNodeList nodes = root.elementsByTagName("include");
    for (int i = 0; i < nodes.count(); i++)
    {
        QDomNode elm = nodes.at(i);
        if (elm.isElement())
        {
            QDomElement e = elm.toElement();
            QString path = e.text();
            path = path.trimmed();
            if (!path.endsWith('/') || !path.endsWith('\\'))
                path = path + "\\";
            includePaths.push_back(path);
            qDebug() << "Include path: " << path;
        }
    }

    // Read sequences
    nodes = root.elementsByTagName("sequence");
    for (int i = 0; i < nodes.count(); i++)
    {
        QDomNode elm = nodes.at(i);
        if (elm.isElement())
        {
            QDomElement e = elm.toElement();
            Sequence s;
            s.loaded = false;
            s.time = 0;
            s.filePattern = e.attribute("filePattern");
            s.start = e.attribute("start", "0").toInt();
            s.stop = e.attribute("stop", "0").toInt();
            s.fps = e.attribute("fps", "0").toInt();
            QString anim = e.attribute("anim", "");
            if (anim == "Repeat")
                s.animation = Animation::Repeat;
            else if (anim == "Mirror")
                s.animation = Animation::Mirror;
            else if (anim == "Once")
                s.animation = Animation::Once;
            else
                s.animation = Animation::Off;
            sequences.push_back(s);
        }
    }
}

QPixmap Structure::loadImage(const Sequence& seq, int index, Eye eye)
{
    QElapsedTimer timer;
    timer.start();
    QString pattern = seq.filePattern;
    pattern = pattern.replace(QString("{eye}"), eye==Eye::Left ? "left" : "right");
    QString path;
    path.sprintf(pattern.toStdString().c_str(), index);
    QString finalPath;
    for (QString incl : includePaths)
    {
        QFile file(incl + path);
        if (file.exists())
        {
            finalPath = incl + path;
            break;
        }
    }
    QFile file(finalPath);
    if (file.exists())
    {
        //load that file
        qDebug() << "Load \"" << file.fileName() << "\"";
        QPixmap img(file.fileName());
        qDebug() << "Loaded in " << timer.elapsed() << "ms: " << img;
        return img;
    }
    else
    {
        qDebug() << "No file \"" << path << "\" found in the include paths";
        return QPixmap();
    }
}

void Structure::loadSequence(Sequence* seq, Eye eye)
{
    if (seq->loaded) return;

    seq->images.resize(seq->stop - seq->start + 1);
#pragma omp parallel for
    for (int i = seq->start; i <= seq->stop; ++i)
    {
        seq->images[i - seq->start] = loadImage(*seq, i, eye);
    }

    seq->loaded = true;
}

void Structure::unloadSequence(Sequence* seq)
{
    seq->images.clear();
    seq->images.shrink_to_fit();
    seq->loaded = false;
}
