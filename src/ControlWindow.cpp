#include <ControlWindow.h>

ControlWindow::ControlWindow(const QString& configFile,
    const QString& leftHost, quint16 leftPort, const QString& rightHost, quint16 rightPort,
    QWidget* parent)
: QMainWindow(parent)
, structure(configFile)
, tcpSocketLeft(nullptr)
, tcpSocketRight(nullptr)
, timer(nullptr)
{    
    //open sockets
    tcpSocketLeft = new QTcpSocket(this);
    streamLeft.setDevice(tcpSocketLeft);
    streamLeft.setVersion(QDataStream::Qt_5_0);
    connect(tcpSocketLeft, &QIODevice::readyRead, this, &ControlWindow::messageReceivedLeft);
    connect(tcpSocketLeft, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
        this, &ControlWindow::displayErrorLeft);
    qDebug() << "Try to connect to left display";
    tcpSocketLeft->connectToHost(leftHost, leftPort);
    if (!tcpSocketLeft->waitForConnected())
    {
        qDebug() << "Unable to open connection to the left display";
        close();
        return;
    }
    qDebug() << "Connection to the left display established";
    
    tcpSocketRight = new QTcpSocket(this);
    streamRight.setDevice(tcpSocketRight);
    streamRight.setVersion(QDataStream::Qt_5_0);
    connect(tcpSocketRight, &QIODevice::readyRead, this, &ControlWindow::messageReceivedRight);
    connect(tcpSocketRight, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error),
        this, &ControlWindow::displayErrorRight);
    qDebug() << "Try to connect to right display";
    tcpSocketRight->connectToHost(rightHost, rightPort);
    if (!tcpSocketRight->waitForConnected())
    {
        qDebug() << "Unable to open connection to the right display";
        close();
        return;
    }
    qDebug() << "Connection to the right display established";

    //layout
    timeWidget = new QLabel("00:00", this);
    currentSequenceWidget = new QLabel("sequence: 0", this);
    currentStepWidget = new QLabel("step: 0", this);
    leftLoadedWidget = new QLabel("left: done", this);
    rightLoadedWidget = new QLabel("left: done", this);
    QFont f0("Arial", 50, QFont::Bold);
    timeWidget->setFont(f0);
    QFont f1("Arial", 30, QFont::Bold);
    currentSequenceWidget->setFont(f1);
    currentStepWidget->setFont(f1);
    QFont f2("Arial", 20, QFont::Bold);
    leftLoadedWidget->setFont(f2);
    rightLoadedWidget->setFont(f2);

    QVBoxLayout *layout = new QVBoxLayout();
    layout->addWidget(timeWidget);
    layout->addWidget(currentSequenceWidget);
    layout->addWidget(currentStepWidget);
    layout->addWidget(leftLoadedWidget);
    layout->addWidget(rightLoadedWidget);
    QWidget* container = new QWidget;
    container->setLayout(layout);
    setCentralWidget(container);
    setWindowTitle("Control");
    setMinimumSize(400, 300);

    //shortcuts
    quitShortcut = new QShortcut(QKeySequence("Esc"), this);
    connect(quitShortcut, SIGNAL(activated()), this, SLOT(shortcutQuit()));
    previousShortcut1 = new QShortcut(QKeySequence("Left"), this);
    connect(previousShortcut1, SIGNAL(activated()), this, SLOT(shortcutPrev()));
    previousShortcut2 = new QShortcut(QKeySequence("PgUp"), this);
    connect(previousShortcut2, SIGNAL(activated()), this, SLOT(shortcutPrev()));
    nextShortcut1 = new QShortcut(QKeySequence("Right"), this);
    connect(nextShortcut1, SIGNAL(activated()), this, SLOT(shortcutNext()));
    nextShortcut2 = new QShortcut(QKeySequence("PgDown"), this);
    connect(nextShortcut2, SIGNAL(activated()), this, SLOT(shortcutNext()));
    resetTimeShortcut = new QShortcut(QKeySequence("r"), this);
    connect(resetTimeShortcut, SIGNAL(activated()), this, SLOT(shortcutResetTime()));

    //start loading
    currentSequence = 0;
    currentStep = 0;
    numLoaded = 0;
    loadingTime = 1;
    shownLeft = true;
    shownRight = true;

    sendLoad(0);
    sendLoad(1);
    sendShow(0, structure.sequences[0].start);

    checkLoadedTimer = new QTimer(this);
    checkLoadedTimer->setSingleShot(false);
    checkLoadedTimer->setInterval(1000 / 10);
    connect(checkLoadedTimer, SIGNAL(timeout()), this, SLOT(checkLoaded()));
    checkLoadedTimer->start();
    displaysLoaded = 0;

    currentTime.start();
}

void ControlWindow::messageReceivedLeft()
{
    while (true) {
        Message m;
        if (Message::readMessage(streamLeft, m))
        {
            //qDebug() << "Message " << static_cast<int>(m.type) << ":" << m.arg1 << "," << m.arg2 << " received";
            switch (m.type)
            {
            case Message::Type::ImageShown:
            {
                if (m.arg1 == currentSequence && m.arg2 == currentStep - structure.sequences[currentSequence].start)
                {
                    shownLeft = true;
                } else
                {
                    qDebug() << "Received faulty ImageShown message from left display";
                }
            }break;
            case Message::Type::IsLoadedAnswer:
            {
                bool loaded = m.arg1 > 0;
                leftLoadedWidget->setText(loaded ? "left: done" : "left: loading");
                leftLoadedWidget->setStyleSheet(loaded ? "QLabel { color : green; }" : "QLabel { color : red; }");
                displaysLoaded = (displaysLoaded & 1) | (loaded ? 1 : 0);
            }break;
            }
        }
        else
        {
            return;
        }
    }
}

void ControlWindow::messageReceivedRight()
{
    while (true) {
        Message m;
        if (Message::readMessage(streamRight, m))
        {
            //qDebug() << "Message " << static_cast<int>(m.type) << ":" << m.arg1 << "," << m.arg2 << " received";
            switch (m.type)
            {
            case Message::Type::ImageShown:
            {
                if (m.arg1 == currentSequence && m.arg2 == currentStep - structure.sequences[currentSequence].start)
                {
                    shownRight = true;
                }
                else
                {
                    qDebug() << "Received faulty ImageShown message from right display";
                }
            }break;
            case Message::Type::IsLoadedAnswer:
            {
                bool loaded = m.arg1 > 0;
                rightLoadedWidget->setText(loaded ? "right: done" : "right: loading");
                rightLoadedWidget->setStyleSheet(loaded ? "QLabel { color : green; }" : "QLabel { color : red; }");
                displaysLoaded = (displaysLoaded & 2) | (loaded ? 2 : 0);
            }break;
            }
        }
        else
        {
            return;
        }
    }
}

void ControlWindow::sendClose()
{
    Message m = { Message::Type::Close, 0, 0 };
    if (tcpSocketLeft && tcpSocketLeft->state()==QAbstractSocket::ConnectedState) Message::writeMessage(tcpSocketLeft, m);
    if (tcpSocketRight && tcpSocketRight->state() == QAbstractSocket::ConnectedState) Message::writeMessage(tcpSocketRight, m);
    //tcpSocketLeft->disconnectFromHost();
    //tcpSocketRight->disconnectFromHost();
    qDebug() << "send close command";
}

void ControlWindow::sendLoad(int sequence)
{
    //first check for unloading
    const int maxLoaded = 2;
    if (numLoaded >= maxLoaded)
    {
        //unload oldest sequence
        int oldestAge = INT_MAX;
        int seq = -1;
        for (int i = 0; i<structure.sequences.size(); ++i)
        {
            if (structure.sequences.at(i).loaded && structure.sequences.at(i).time < oldestAge)
            {
                seq = i;
                oldestAge = structure.sequences.at(i).time;
            }
        }
        if (seq >= 0)
        {
            sendUnload(seq);
            structure.sequences.at(seq).loaded = false;
            numLoaded--;
        }
    }

    //now load that sequence
    Message m = { Message::Type::LoadSequence, sequence, 0 };
    Message::writeMessage(tcpSocketLeft, m);
    Message::writeMessage(tcpSocketRight, m);
    structure.sequences.at(sequence).loaded = true;
    structure.sequences.at(sequence).time = loadingTime++;
    numLoaded++;
    qDebug() << "send load command: " << sequence;
}

void ControlWindow::sendUnload(int sequence)
{
    Message m = { Message::Type::UnloadSequence, sequence, 0 };
    Message::writeMessage(tcpSocketLeft, m);
    Message::writeMessage(tcpSocketRight, m);
    qDebug() << "send unload command: " << sequence;
}

void ControlWindow::sendShow(int sequence, int step)
{
    shownLeft = false;
    shownRight = false;
    Message m = { Message::Type::ShowImage, sequence, step - structure.sequences[sequence].start };
    Message::writeMessage(tcpSocketLeft, m);
    Message::writeMessage(tcpSocketRight, m);
    qDebug() << "send show command: " << sequence << "," << step;
    currentSequenceWidget->setText(QString().sprintf("sequence: %d", sequence));
    currentStepWidget->setText(QString().sprintf("step: %d", step));
}

void ControlWindow::sendTestLoaded(int sequence)
{
    Message m = { Message::Type::IsLoadedRequest, sequence, 0 };
    Message::writeMessage(tcpSocketLeft, m);
    Message::writeMessage(tcpSocketRight, m);
    //qDebug() << "send test command";
}

void ControlWindow::closeEvent(QCloseEvent* bar)
{
    sendClose();
    QMainWindow::closeEvent(bar);
}

void ControlWindow::displayErrorLeft(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The host was not found. Please check the "
                "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The connection was refused by the peer. "
                "Make sure the fortune server is running, "
                "and check that the host name and port "
                "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The following error occurred: %1.")
            .arg(tcpSocketLeft->errorString()));
    }
}
void ControlWindow::displayErrorRight(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The host was not found. Please check the "
                "host name and port settings."));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The connection was refused by the peer. "
                "Make sure the fortune server is running, "
                "and check that the host name and port "
                "settings are correct."));
        break;
    default:
        QMessageBox::information(this, tr("Fortune Client"),
            tr("The following error occurred: %1.")
            .arg(tcpSocketRight->errorString()));
    }
}

void ControlWindow::shortcutQuit()
{
    sendClose();
    close();
}

void ControlWindow::shortcutNext()
{
    //if (!shownLeft || !shownRight) return;
    qDebug() << "next";
    bool sequenceChanged = false;
    if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror
        || structure.sequences[currentSequence].animation == Structure::Animation::Repeat
        || (structure.sequences[currentSequence].animation == Structure::Animation::Once
            && currentStep > structure.sequences[currentSequence].start))
    {
        //stop animation
        if (displaysLoaded) {
            currentSequence++;
            sequenceChanged = true;
        }
    }
    else
    {
        if (displaysLoaded || structure.sequences[currentSequence].stop >= currentStep + 1) {
            currentStep++;
        }
        if (structure.sequences[currentSequence].stop < currentStep)
        {
            //done with that sequence
            currentSequence++;
            sequenceChanged = true;
        }
    }

    if (currentSequence >= structure.sequences.size())
    {
        currentSequence = structure.sequences.size() - 1;
        return;
    }

    if (sequenceChanged)
    {
        //stop timer
        if (timer)
        {
            timer->stop();
            delete timer;
            timer = nullptr;
        }

        //enter next sequence
        currentStep = structure.sequences[currentSequence].start;
        if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror
            || structure.sequences[currentSequence].animation == Structure::Animation::Repeat)
        {
            //start timer
            timer = new QTimer(this);
            timer->setTimerType(Qt::PreciseTimer);
            timer->setInterval(1000 / structure.sequences[currentSequence].fps);
            timer->setSingleShot(false);
            timerState = 0;
            connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
            timer->start();
        }
        
        //load next sequence
        if (structure.sequences.size() > currentSequence + 1)
            sendLoad(currentSequence + 1);
    }
    if (currentStep == structure.sequences[currentSequence].start + 1
        && structure.sequences[currentSequence].animation == Structure::Animation::Once)
    {
        //special case: start 'Once'-Animation from the second frame
        //start timer
        timer = new QTimer(this);
        timer->setTimerType(Qt::PreciseTimer);
        timer->setInterval(1000 / structure.sequences[currentSequence].fps);
        timer->setSingleShot(false);
        timerState = 0;
        connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
        timer->start();
    }

    sendShow(currentSequence, currentStep);
}

void ControlWindow::shortcutPrev()
{
    //if (!shownLeft || !shownRight) return;
    qDebug() << "previous";

    if (currentSequence == 0 && currentStep == structure.sequences[currentSequence].start)
    {
        return; //already at the beginning
    }

    //stop timer
    if (timer)
    {
        timer->stop();
        delete timer;
        timer = nullptr;
    }

    bool sequenceChanged = false;
    if (structure.sequences[currentSequence].animation == Structure::Animation::Once
        && currentStep >= structure.sequences[currentSequence].start + 1)
    {
        //jump to the start of the Once-animation to start it again
        currentStep = structure.sequences[currentSequence].start;
    } 
    else if (structure.sequences[currentSequence].animation == Structure::Animation::Off
        && structure.sequences[currentSequence].start < currentStep) {
        //go one step back
        currentStep--;
    }

    //switching to the previous sequence does not work with the preloading.
    //Therefore, I don't allow it.

    //else if (structure.sequences[currentSequence].animation != Structure::Animation::Off) {
    //    //stop animation
    //    currentSequence--;
    //    sequenceChanged = true;
    //}
    //else
    //{
    //    currentStep--;
    //    if (structure.sequences[currentSequence].start > currentStep)
    //    {
    //        //done with that sequence
    //        currentSequence--;
    //        sequenceChanged = true;
    //    }
    //}

    //if (sequenceChanged)
    //{
    //    //switch to previous sequence
    //    if (structure.sequences[currentSequence].animation != Structure::Animation::Off)
    //    {
    //        //jump to the start of that animation
    //        currentStep = structure.sequences[currentSequence].start;
    //        //start animation again?
    //        if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror
    //            || structure.sequences[currentSequence].animation == Structure::Animation::Repeat)
    //        {
    //            //start timer
    //            timer = new QTimer(this);
    //            timer->setTimerType(Qt::PreciseTimer);
    //            timer->setInterval(1000 / structure.sequences[currentSequence].fps);
    //            timer->setSingleShot(false);
    //            timerState = 0;
    //            connect(timer, SIGNAL(timeout()), this, SLOT(timerUpdate()));
    //            timer->start();
    //        }
    //    }
    //    else
    //    {
    //        //jump to the end of that animation
    //        currentStep = structure.sequences[currentSequence].stop;
    //    }
    //}

    sendShow(currentSequence, currentStep);
}

void ControlWindow::shortcutResetTime()
{
    currentTime.restart();
}

void ControlWindow::timerUpdate()
{
    if (!shownLeft || !shownRight) return;
    if (timerState == 0)
    {
        //forward
        if (structure.sequences[currentSequence].stop <= currentStep)
        {
            if (structure.sequences[currentSequence].animation == Structure::Animation::Mirror)
            {
                timerState = 1;
                currentStep--;
            }
            else if (structure.sequences[currentSequence].animation == Structure::Animation::Repeat)
            {
                currentStep = structure.sequences[currentSequence].start;
            }
            else
            {
                if (timer) {
                    timer->stop();
                    delete timer;
                    timer = nullptr;
                }
            }
        }
        else
        {
            currentStep++;
        }
    }
    else
    {
        //backward
        if (currentStep > structure.sequences[currentSequence].start)
        {
            currentStep--;
        }
        else
        {
            currentStep = structure.sequences[currentSequence].start + 1;
            timerState = 0;
        }
    }
    
    sendShow(currentSequence, currentStep);
}

void ControlWindow::checkLoaded()
{
    sendTestLoaded(currentSequence);
    int msec = currentTime.elapsed();
    QTime t(0,0);
    t = t.addMSecs(msec);
    timeWidget->setText(t.toString("mm:ss"));
}
