#ifndef STRUCTURE_MANAGER_H
#define STRUCTURE_MANAGER_H

#include <Structure.h>

#include <vector>
#include <QtCore>
#include <QPixmap>
#include <memory>
#include <queue>

class StructureManager : public QObject
{
    Q_OBJECT;

public:
    StructureManager(const QString& configFile);

    enum class Eye
    {
        Left, Right
    };
    void setEye(Eye eye);

    enum Options
    {
        LoadLeft = 1,
        LoadRight = 2
    };
    void setOptions(int options);

    void start();

public slots:
    void clickLeft();
    void clickRight();
    void clickPauseResume();
    void close();

private slots:
    void timerUpdate();

signals:
    void nextImage(QPixmap img);

private:
    void sendCurrentImage();

    struct LoadedSequence
    {
        std::vector<QPixmap> images[2];
        bool loaded = false;
        int time = 0;
        QMutex mutex;
        QWaitCondition condition;
    };
    std::unique_ptr<std::vector<LoadedSequence>> loadedSequences;

    class LoadingThread : public QThread
    {
    public:
        LoadingThread(StructureManager* manager, QObject* parent = nullptr);
        void run() override;
        void loadSequence(int sequence);

    private:
        StructureManager* manager;
        QMutex mutex;
        QWaitCondition condition;
        std::queue<int> nextSequence;

        QPixmap load(const QString& pattern, int index, const QString& eye);
    };
    LoadingThread* loadingThread;

    Eye eye;
    int options;
    Structure structure;
    int currentSequence;
    int currentStep;
    std::vector<QString> includePaths;

    QTimer* timer;
    int timerState;

    friend class LoadingThread;
};

#endif