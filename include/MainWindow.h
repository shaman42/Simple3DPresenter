#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtWidgets>
#include <StructureManager.h>
#include <QPixmap>

class MainWindow : public QMainWindow {
	Q_OBJECT;

public:
	MainWindow(const QString& configFile, const QString& eye, QWidget* parent = nullptr);

public slots:
    void nextImage(QPixmap img);

private:
    StructureManager m;
    QPixmap currentImg;
    QLabel* label;

    QShortcut* quitShortcut;
    QShortcut* nextShortcut;
    QShortcut* previousShortcut;
    QShortcut* spaceShortcut;
};

#endif