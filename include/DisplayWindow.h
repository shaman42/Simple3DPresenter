#ifndef DISPLAY_WINDOW_H
#define DISPLAY_WINDOW_H

#include <QtWidgets>
#include <QtNetwork>
#include <QPixmap>

#include <Structure.h>
#include <Message.h>
#include <queue>


class DisplayWindow : public QMainWindow {
    Q_OBJECT;

public:
    DisplayWindow(const QString& configFile, 
        Structure::Eye eye, quint16 port,
        QWidget* parent = nullptr);

private slots:
    void newConnection();
    void messageReceived();
    void displayError(QAbstractSocket::SocketError socketError);

protected:
    void closeEvent(QCloseEvent *bar) override;

private:
    Structure::Eye eye;
    Structure structure;
    QTcpServer *tcpServer;
    QTcpSocket *tcpSocket;
    QDataStream in;

    QPixmap currentImg;
    QLabel* label;

    class LoadingThread : public QThread
    {
    public:
        LoadingThread(DisplayWindow* manager, QObject* parent = nullptr);
        void run() override;
        void loadSequence(int sequence);

    private:
        DisplayWindow* manager;
        QMutex mutex;
        QWaitCondition condition;
        std::queue<int> nextSequence;
    };
    LoadingThread* loadingThread;
    friend class LoadingThread;
};


#endif