#ifndef MESSAGE_H
#define MESSAGE_H

#include <QDataStream>

struct Message
{
    enum class Type
    {
        /**
         * \brief Closes the application. No arguments
         */
        Close,
        /**
         * \brief Loads a sequence. Argument: 1x int, the sequence to load
         */
        LoadSequence,

        /**
         * \brief Unloads a sequence. Argument: 1x int, the sequence to unload
         */
        UnloadSequence,

        /**
         * \brief Shows the image. Argument: 2x int, sequence number, image number
         */
        ShowImage,
        ImageShown,

        /**
         * \brief Request an answer of type IsLoadedAnswer from the displays. Argument: 1x int, the sequence to check
         */
        IsLoadedRequest,

        /**
         * \brief The answer. Argument: 1x int, >0 if this sequence is loaded
         */
        IsLoadedAnswer
    };

public:
    Type type;
    int arg1;
    int arg2;

    static bool readMessage(QDataStream& in, Message& m)
    {
        in.startTransaction();

        int type, arg;
        in >> type;
        m.type = static_cast<Type>(type);
        switch (m.type)
        {
        case Type::Close:
        {
            m.arg1 = 0;
            m.arg2 = 0;
        }break;
        case Type::LoadSequence:
        case Type::UnloadSequence:
        case Type::IsLoadedAnswer:
        case Type::IsLoadedRequest:
        {
            in >> arg;
            m.arg1 = arg;
            m.arg2 = 0;
        }break;
        case Type::ShowImage:
        case Type::ImageShown:
        {
            in >> arg;
            m.arg1 = arg;
            in >> arg;
            m.arg2 = arg;
        }break;
        }

        return in.commitTransaction();
    }

    static void writeMessage(QTcpSocket* socket, const Message& m)
    {
        QByteArray block;
        QDataStream out(&block, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_5_0);

        out << static_cast<int>(m.type);
        if (m.type != Type::Close)     out << m.arg1;
        if (m.type == Type::ShowImage || m.type==Type::ImageShown) out << m.arg2;

        socket->write(block); socket->flush();
    }
};


#endif