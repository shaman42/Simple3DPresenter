#ifndef STRUCTURE_H
#define STRUCTURE_H

#include <QString>
#include <vector>
#include <QPixmap>

/**
 * \brief The structure of the presentation, as loaded from the XML
 */
class Structure
{
public:
    enum class Eye
    {
        Left, Right
    };

    enum class Animation
    {
        Off,
        Once,
        Repeat,
        Mirror
    };

    struct Sequence
    {
        QString filePattern;
        int start;
        int stop;
        int fps;
        Animation animation;

        std::vector<QPixmap> images;
        bool loaded = false;
        int time = 0;
    };

    std::vector<Sequence> sequences;
    std::vector<QString> includePaths;

    Structure(const QString& configFile);

    QPixmap loadImage(const Sequence& seq, int index, Eye eye);
    void loadSequence(Sequence* seq, Eye eye);
    void unloadSequence(Sequence* seq);
};


#endif
