#ifndef CONTROL_WINDOW_H
#define CONTROL_WINDOW_H

#include <QtWidgets>
#include <QtNetwork>
#include <QPixmap>

#include <Structure.h>
#include <Message.h>


class ControlWindow : public QMainWindow {
    Q_OBJECT;

public:
    ControlWindow(const QString& configFile, 
        const QString& leftHost, quint16 leftPort, const QString& rightHost, quint16 rightPort,
        QWidget* parent = nullptr);

private:
    void sendClose();
    void sendLoad(int sequence);
    void sendUnload(int sequence);
    void sendShow(int sequence, int step);
    void sendTestLoaded(int sequence);

private slots:
    void messageReceivedLeft();
    void messageReceivedRight();
    void displayErrorLeft(QAbstractSocket::SocketError socketError);
    void displayErrorRight(QAbstractSocket::SocketError socketError);
    void shortcutQuit();
    void shortcutNext();
    void shortcutPrev();
    void shortcutResetTime();
    void timerUpdate();
    void checkLoaded();

protected:
    void closeEvent(QCloseEvent *bar) override;

private:
    Structure structure;

    QTcpSocket *tcpSocketLeft;
    QDataStream streamLeft;
    QTcpSocket *tcpSocketRight;
    QDataStream streamRight;

    QShortcut* quitShortcut;
    QShortcut* nextShortcut1;
    QShortcut* nextShortcut2;
    QShortcut* previousShortcut1;
    QShortcut* previousShortcut2;
    QShortcut* resetTimeShortcut;

    QLabel* timeWidget;
    QLabel* currentSequenceWidget;
    QLabel* currentStepWidget;
    QLabel* leftLoadedWidget;
    QLabel* rightLoadedWidget;

    int currentSequence;
    int currentStep;
    bool shownLeft, shownRight;
    QTimer* timer;
    int timerState;
    int loadingTime;
    int numLoaded;

    QTimer* checkLoadedTimer;
    int displaysLoaded;
    
    QTime currentTime;
};


#endif