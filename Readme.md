# Simple 3D Presenter for a stereoscopic monitor system

The presentation tool is tailored towards the following system:
- Two computers connected by network, each is responsible for one eye
- A third computer acting as the controller

See the folder *example-...* for an example application

![Screenshot of the example](Example.png "Example.png")

Features / Keys:
- presentation specified as an XML:
  - two images per slide: for the left and the right eye
  - Animations: repeat, mirror, only-once, controllable speed
- Switch between the slides with left and right click or page up / page down on your keyboard
- Click 'r' to reset the timer
- Escape to close the presentation (on all three machines)

Note: No warranties that this software works.
This was written for one single seminar presentation within three hours.
It will not be developed further and still contains many bugs.

You're free to use and modify the code, see license.

## How to create 3D Presentations:

The folder *example-latex* contains an example latex presentation.

I defined two macros that control the proper translation so that text or TikZ pictures are displayed in 3D.
Another macro simplifies the usage of stereo images.
See the file example-latex/DemoPresentation.tex for details.

You have to render two PDFs from the presentation, one for each eye.
Let's assume you've called them *Left.pdf* and *Right.pdf*

The next step is to convert them to single images:
    magick -density 650 reft.pdf -quality 100 -resize 3840x2160! left.png
    magick -density 650 Right.pdf -quality 100 -resize 3840x2160! right.png
Here, I used ImageMagick, but any other software to convert PDF to images can be used.
The result can be seen in *example-presentation/*.

Finally, you have to define the structure of the presentation: Where are animations, what kind of animation, file names and patterns, and so on.
See example-presentation/PresentationStructure.xml for details

## How to launch the presentation

On the computer for the left eye: `Presenter.exe -f PresentationStructure.xml left`. The option parameter `-f` switches the presenter from windowed mode to fullscreen mode.

Similar for the computer for the right eye: `Presenter.exe -f PresentationStructure.xml right`.

And on the control server: `Presenter.exe PresentationStructure.xml control`.
