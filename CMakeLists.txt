cmake_minimum_required(VERSION 3.2.0)

project(Simple3DPresenter)

# Try to find qt and add it if it is not already in CMAKE_PREFIX_PATH
if(NOT "${CMAKE_PREFIX_PATH}" MATCHES "[Qq][Tt]")
    if(NOT QT_QMAKE_EXECUTABLE)
        find_program(QT_QMAKE_EXECUTABLE_FINDQT NAMES qmake qmake4 qmake-qt4 qmake5 qmake-qt5
           PATHS "${QT_SEARCH_PATH}/bin" "$ENV{QTDIR}/bin")
        set(QT_QMAKE_EXECUTABLE ${QT_QMAKE_EXECUTABLE_FINDQT} CACHE PATH "Qt qmake program.")
    endif()

    if(QT_QMAKE_EXECUTABLE)
        execute_process(COMMAND ${QT_QMAKE_EXECUTABLE} -query "QT_INSTALL_PREFIX" OUTPUT_VARIABLE QT5_PATH OUTPUT_STRIP_TRAILING_WHITESPACE)
    else()
        if(APPLE) # On OSX look for the usual brew installation. 
            foreach(path "/usr/local/Cellar/qt" "/usr/local/Cellar/qt5")
                if(EXISTS ${path})
                    file(GLOB qtversions RELATIVE "${path}/" "${path}/?.?.*")
                    list(LENGTH qtversions len)
                    if(${len} GREATER 0)
                        list(GET qtversions -1 qtlatest)
                        if(EXISTS "${path}/${qtlatest}")
                            set(QT5_PATH "${path}/${qtlatest}")
                            break()
                        endif()
                    endif()
                endif()
            endforeach()
        endif()
    endif()

    if(QT5_PATH)
        ivw_debug_message(STATUS "Found qt at: ${QT5_PATH}") 
        list(APPEND CMAKE_PREFIX_PATH ${QT5_PATH}) 
    endif()
endif()


# Find includes in corresponding build directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)
# Instruct CMake to run moc automatically when needed.
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

# Find the QtWidgets library
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Xml REQUIRED)
find_package(Qt5Network REQUIRED)

FIND_PACKAGE( OpenMP REQUIRED)
if(OPENMP_FOUND)
message("OPENMP FOUND")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

set(SOURCE_FILES 
    src/MainWindow.cpp 
    src/StructureManager.cpp 
    src/Structure.cpp 
    src/DisplayWindow.cpp 
    src/ControlWindow.cpp 
    resources/resources.qrc)
set(HEADER_FILES 
    include/MainWindow.h 
    include/Structure.h 
    include/StructureManager.h
    include/DisplayWindow.h
    include/ControlWindow.h
    include/Message.h)
source_group("Headers" FILES ${HEADER_FILES})

add_executable(Presenter ${SOURCE_FILES} ${HEADER_FILES} src/main.cpp)
target_include_directories(Presenter PUBLIC include)
target_link_libraries(Presenter Qt5::Widgets Qt5::Xml Qt5::Network)